document.addEventListener("DOMContentLoaded", function() {
    var textoAdicional = document.querySelector("#texto ~ .oculto");
    var botonVerMas = document.getElementById("btnver");

    botonVerMas.addEventListener("click", function() {
        if (textoAdicional.style.display === "none" || textoAdicional.style.display === "") {
            // Mostrar el texto con una animación
            textoAdicional.style.display = "block";
            textoAdicional.style.maxHeight = textoAdicional.scrollHeight + "px"; // Animación de altura
            botonVerMas.textContent = "Ver Menos";
        } else {
            // Ocultar el texto con una animación
            textoAdicional.style.maxHeight = "0"; // Animación de altura
            textoAdicional.addEventListener("transitionend", function () {
                textoAdicional.style.display = "none";
            }, { once: true });
            botonVerMas.textContent = "Ver Más";
        }
    });
});

document.addEventListener("DOMContentLoaded", function () {
    const links = document.querySelectorAll('a[href^="#"]');
    
    for (const link of links) {
        link.addEventListener("click", function (e) {
            e.preventDefault();
            const href = this.getAttribute("href");
            const target = document.querySelector(href);
            target.scrollIntoView({
                behavior: "smooth"
            });
        });
    }
});
